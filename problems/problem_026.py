# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

# def calculate_grade(values):
    # average scores is the sum of values divided by the number of values
    # ave_scores = sum(values) / len(values)

    # if ave_scores >= 90:
    #     return "A"
    # elif ave_scores >= 80:
    #     return "B"
    # elif ave_scores >= 70:
    #     return "C"
    # elif ave_scores >= 60:
    #     return "D"
    # else:
    #     return "F"

grade_key = (("A", 90), ("B", 80), ("C", 70), ("D", 60), ("F", 0))

def calculate_grade(values):
    ave_scores = sum(values) / len(values)
    for grade, key in grade_key:
        if ave_scores >= key:
            return grade
