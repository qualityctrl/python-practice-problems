# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    results = []
    for item in csv_lines:
        value = 0
        value = item.split()
        for x in item:
            if x.isdigit():
                value += x
        results.append(x)
        return results




            # z = str(item)
            # item = item + z


print(add_csv_lines(["3", "1,9"]))



    # accepts a list as a a (list) paramater
    #each item is a strings separated by numbers "123", "456"
    # return new list of sum of the strings
