# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):

    for char in password:
        if char in password is char.isdigit() and char.isupper() and char.islower() and char.isalpha() and char <= 6 and char >= 12:
            return True
    else:
        return False


print(check_password("hippopotamus"))

# for the characters in password
# check to see if there is >= 1 lowercase
# check to see if there is >= 1 uppercase
# chest to see if there is >= 1 digit
# check to see if there is >= special char
# check to see if ther eis  char <= 6 char >=12
