# Complete the reverse_dictionary function which has a
# single parameter that is a dictionary. Return a new
# dictionary that has the original dictionary's values
# for its keys, and the original dictionary's keys for
# its values.
#
# Examples:
#   * input:  {}
#     output: {}
#   * input:  {"key": "value"}
#     output: {"value", "key"}
#   * input:  {"one": 1, "two": 2, "three": 3}
#     output: {1: "one", 2: "two", 3: "three"}

def reverse_dictionary(dictionary):
    new_dictionary = {}
    for key, value in dictionary.items():
        new_dictionary[value] = new_dictionary.get(value, [] + [key])
    return new_dictionary

    # return new_dictionary with:
    # 1. original dictionary values for its keys
    # 2. original dictionary keys for its values
    # key and value are swapped and that's how we reverse the dictionary


original_dict = {"apple": "red",
                 "banana": "yellow",
                 "grape": "purple"}

reverse_dict = reverse_dictionary(original_dict)
print(reverse_dict)
