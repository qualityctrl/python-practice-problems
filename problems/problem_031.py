# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_squares(values):

    if values == []:
        return None
    squares = []
    for num in values:
        squares.append(num ** 2)
    return sum(squares)





# if values is an empty list values = [], return None
# squres is an empty list
#iterate through the list and item**2
# list of values and returns sum item **2 or (sum * sum)
# get the sum of item**2 in the list
