# Complete the sum_of_first_n_numbers function which accepts
# a numerical limit and returns the sum of the numbers from
# 0 up to and including limit.
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+1=1
#   * 2 returns 0+1+2=3
#   * 5 returns 0+1+2+3+4+5=15
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.
# import random


def sum_of_first_n_numbers(limit):
    if limit < 0:
        return None

    return sum(range(0, limit+1))



# limit = 12
# test_range = 1
# while len(limit) < test_range:
#     limit.append(random.randrange(50, 100))
# print(sum_of_first_n_numbers(limit))

# make a list that returns sum of the numbers in limit
# Start adding from 0 up to the limit
#if limit is less than 0 return None
