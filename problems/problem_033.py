# Complete the sum_of_first_n_even_numbers function which
# accepts a numerical count n and returns the sum of the
# first n even numbers
#
# If the value of the limit is less than 0, then it should
# return None
#
# Examples:
#   * -1 returns None
#   * 0 returns 0
#   * 1 returns 0+2=2
#   * 2 returns 0+2+4=6
#   * 5 returns 0+2+4+6+8+10=30
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_first_n_even_numbers(n):
    even_numbers = []
    if n < 0:
        return None
    numbers = range(n*2+1)
    # print(numbers) here to see what numbers is giving
    for num in numbers:
        if num % 2 == 0:
            even_numbers.append(num)
    #print(even_numbers).append(num)
    #or print(num) to see what num is.
    return sum(even_numbers)






# if value is less than 0; return None
#count_n return the sum of the number n
