# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you
import random


def calculate_average(values):
    if len(values) > 0:
        return sum(values) / len(values)
    else:
        if len(values) == 0:
            return None
        # if bool[values] == False
        # this is a second option to this equation

# values = [2, 4, 5, 6]
# print(calculate_average(values))


values = []
test_range = 12
while len(values) < test_range:
    values.append(random.randrange(50, 100))
print(calculate_average(values))
