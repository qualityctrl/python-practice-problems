# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):

    if values == [] or len(values) == 1:
        return None



# if length(values) <= 1:  (second option)
#     return None

    # values = [53, 92, 68, 90, 70, 66, 73, 85, 70, 74, 68, 73]
    values = values.sort()
    return values[-2]
