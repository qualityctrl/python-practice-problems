# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
# #
# import random

import random


def max_in_list(values):
    if len(values) == 0:
        return None

    return max(values)


values = []
test_range = 12
while len(values) < test_range:
    values.append(random.randrange(50, 100))
print(max_in_list(values))
print(values)
