# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
import random


def calculate_sum(values):
    if len(values) == 0:
        # if len of values is equal to zero
        # values == false
        return None
    # else: is optional
    return sum(values)


values = []
test_range = 12
while len(values) < test_range:
    values.append(random.randrange(50, 100))
print(calculate_sum(values))
